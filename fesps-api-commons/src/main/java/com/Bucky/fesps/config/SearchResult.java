package com.Bucky.fesps.config;

import lombok.Data;

/**
 * @author Danny
 * @date 2022-05-16
 */
@Data
public class SearchResult<T>{
    private String code;
    private String msg;
    private T data;
    private Long total;

    public SearchResult(T data,Long total) {
        this.data=data;
        this.total=total;
    }

    public SearchResult() {
    }

    public static SearchResult success() {
        SearchResult result = new SearchResult();
        result.setCode("0");
        result.setMsg("操作成功");
        return result;
    }

    public static <T> SearchResult<T> success(T data,Long total) {
        SearchResult result = new SearchResult<T>(data,total);
        result.setCode("0");
        result.setMsg("操作成功");
        return result;
    }

    public static SearchResult error(String code,String msg){
        SearchResult result = new SearchResult();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
