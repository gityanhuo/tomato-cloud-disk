package com.Bucky.fesps.config;

import lombok.Data;

/**
 * @author Danny
 * @date 2022-03-17
 */
@Data
public class Result<T> {
    private String code;
    private String msg;
    private T data;

    public Result(T data) {
        this.data=data;
    }

    public Result() {
    }

    public static Result success() {
        Result result = new Result();
        result.setCode("0");
        result.setMsg("操作成功");
        return result;
    }

    public static <T> Result<T> success(T data) {
        Result result = new Result<>(data);
        result.setCode("0");
        result.setMsg("操作成功");
        return result;
    }

    public static Result error(String code,String msg){
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
