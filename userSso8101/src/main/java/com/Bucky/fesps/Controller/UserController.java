package com.Bucky.fesps.Controller;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.entity.BasicUser;
import com.Bucky.fesps.entity.User;
import com.Bucky.fesps.mapper.UserMapper;
import com.Bucky.fesps.service.CookieService;
import com.Bucky.fesps.service.SSOService;
import com.Bucky.fesps.utils.CookieUtil;
import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.Bucky.fesps.utils.GetIpUtil.getIpAddress;
import static com.Bucky.fesps.utils.Md5Util.convertMD5;
import static com.Bucky.fesps.utils.Md5Util.string2MD5;

/**
 * @author Danny
 * @date 2022-03-24
 */
@Slf4j

@RestController
@RequestMapping("/user")
public class UserController {

    private final SSOService ssoService;
    private final CookieService cookieService;
    @Resource
    UserMapper userMapper;

    public UserController(SSOService ssoService, CookieService cookieService, RedisTemplate<String, String> redisTemplate) {
        this.ssoService = ssoService;
        this.cookieService = cookieService;
//        this.redisTemplate = redisTemplate;
    }

    @ResponseBody
    @PostMapping("/login")
    public Result login(@RequestBody BasicUser user, HttpServletRequest request, HttpServletResponse response) {
        //登陆时调用ssoService的login生成token
        String token = ssoService.login(user).getData();
        //如果生成成功，将票据写入cookie
        if (!StringUtils.isEmpty(token)) {
            //将信息存入session中，用于页面返显
            request.getSession().setAttribute("user", user);

            boolean result = cookieService.setCookie(request, response, token);

            //写入登录ip
            String ip = getIpAddress(request);
            User updateUser = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()));
            updateUser.setLastIp(ip);
            updateUser.setToken(token);
            log.info(user.getUsername() + "在" + ip + "地址登录");
            userMapper.updateById(updateUser);
            return result ? Result.success() : Result.error("-1", "token生成失败");
        }

        return Result.error("-1", "token生成失败");
    }

    @ResponseBody
    @GetMapping("/logout")
    public Result logout(HttpServletRequest request) {
        //从cookie中取出token的值
        String token = CookieUtil.getCookieValue(request, "UserToken");
        //删除用户的token
        Result result = ssoService.logout(token);
        return result;
    }

    @ResponseBody
    @PostMapping("/register")
    public Result register(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
        User res = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()));
        if (res != null) {
            return Result.error("-1", "用户名已存在");
        }
        String secretKey = convertMD5(string2MD5(user.getPassword()));
        user.setSecretKey(secretKey);

        BasicUser basicUser = new BasicUser();
        basicUser.setUsername(user.getUsername());
        basicUser.setPassword(user.getPassword());
        user.setPassword(null);
        int insert = userMapper.insert(user);
        log.info(user.getUsername() + "用户注册成功");
        //注册成功后直接登录
        Result loginResult = login(basicUser, request, response);
        return loginResult;
    }

    @ResponseBody
    @PutMapping("/update")
    public Result update(@RequestBody User user, HttpServletRequest request) {
        //验证用户信息
        Result result = validate(request);
        if (!result.getCode().equals("-1")) {
            userMapper.updateById(user);
            log.info(user.toString() + "更新成功");
            return Result.success();
        }
        return Result.error("-1", "用户更新失败");
    }

    //验证token返回用户信息
    @ResponseBody
    @GetMapping("/validate")
    public Result validate(HttpServletRequest request) {
        //从cookie中取出token的值
        String token = CookieUtil.getCookieValue(request, "UserToken");
        //再使用token从redis中查看是否在线，并返回用户信息
        Result<User> userValidate = ssoService.validate(token);
        return userValidate;
    }


//    @PostMapping("/insert")
//    public Result save(@RequestBody User user) {
//        userMapper.insert(user);
//        log.info(user.toString() + "写入成功");
//        return Result.success();
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public Result delete(@PathVariable Long id) {
//        userMapper.deleteById(id);
//        log.info(id + "用户删除成功");
//        return Result.success();
//    }


//    @GetMapping("/select")
//    public Result findPage(@RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "") String search) {
//
//        LambdaQueryWrapper<User> wrapper = Wrappers.<User>lambdaQuery();
//        if (!StrUtil.isBlank(search)) {
//            wrapper.like(User::getNickName, search);
//        }
//        Page<User> nickNamePage = userMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
//        //Page<User> addressPage = userMapper.selectPage(new Page<>(pageNum, pageSize), Wrappers.<User>lambdaQuery().like(User::getAddress, search));
//
//        return Result.success(nickNamePage);
//    }

    @GetMapping("/test")
    public String getTest() {

        log.info("注解的日志测试");
        return "测试controller";
    }
}