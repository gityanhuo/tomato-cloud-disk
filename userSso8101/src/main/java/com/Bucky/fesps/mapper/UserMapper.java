package com.Bucky.fesps.mapper;

import com.Bucky.fesps.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Danny
 * @date 2022-03-17
 */
public interface UserMapper extends BaseMapper<User> {
}
