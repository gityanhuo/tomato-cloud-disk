package com.Bucky.fesps.service;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.entity.BasicUser;
import com.Bucky.fesps.entity.User;

/**
 * @author Danny
 * @date 2022-03-22
 * 单点登录服务
 */
public interface SSOService {

    //登录认证方法返回token
    Result<String> login(BasicUser user);

    //验证token返回用户信息
    Result<User> validate(String token);

    //登出删除session
    Result logout(String token);

}
