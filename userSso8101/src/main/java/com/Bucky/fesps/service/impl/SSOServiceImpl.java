package com.Bucky.fesps.service.impl;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.entity.BasicUser;
import com.Bucky.fesps.entity.User;
import com.Bucky.fesps.mapper.UserMapper;
import com.Bucky.fesps.service.SSOService;
import com.Bucky.fesps.utils.JsonUtil;
import com.Bucky.fesps.utils.UUIDUtil;
import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

import static com.Bucky.fesps.utils.Md5Util.convertMD5;
import static com.Bucky.fesps.utils.Md5Util.string2MD5;

/**
 * @author Danny
 * @date 2022-03-22
 * 单点登录实现类
 */
@Slf4j
@Service
public class SSOServiceImpl implements SSOService {

    @Resource
    UserMapper userMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //登录认证方法，返回token
    @Override
    public Result<String> login(BasicUser user) {
        //判断参数合法性
        if (StringUtils.isEmpty(user.getUsername().trim())) {
            return Result.error("-1", "用户名为空");
        }
        if (StringUtils.isEmpty(user.getPassword().trim())) {
            return Result.error("-1", "密码为空");
        }

        User res = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()));
        if (res == null) {
            log.info(user.toString() + "用户不存在");
            return Result.error("-1", "用户不存在");
        }
        //判断输入密码转换成密钥后和数据库的密钥是否一致
        boolean judge = convertMD5(string2MD5(user.getPassword())).equals(res.getSecretKey());
        if (judge) {
            //查询是否在线,如果在线就删除已在线的token，使其下线
            if (res.getToken() != null) {
                logout(res.getToken());
            }
            ;

            //生成token存入redis，页面使用cookie存储用户信息
            ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
            String token = UUIDUtil.getUUID();
            log.info(user.getUsername() + "正在登陆,返回token");
            //将生成的token和转换成json的user信息存入redis，并设置有效时间30分钟
            res.setSecretKey(null);
            valueOperations.set(token, JsonUtil.getInstance().toJson(res), 300, TimeUnit.MINUTES);
            return Result.success(token);
        }
        return Result.error("-1", "用户名或密码错误");

    }

    //验证token返回用户信息
    @Override
    public Result<User> validate(String token) {
        if (StringUtils.isEmpty(token)) {
            log.info("token为空");
            return Result.error("-1", "token为空");
        }
        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        String userJson = valueOperations.get(token);
        if (StringUtils.isEmpty(userJson)) {
            log.info("用户信息不存在");
            return Result.error("-1", "用户信息不存在");
        }
        User user = JsonUtil.getInstance().jsonToObject(userJson, User.class);
        return Result.success(user);
    }


    //登出删除session
    @Override
    public Result logout(String token) {
        if (StringUtils.isEmpty(token)) {
            log.info("token为空");
            return Result.error("-1", "token为空");
        }
        String userJson = redisTemplate.opsForValue().get(token);
        if(userJson==null){
            return Result.success();
        }
        //删除redis中该用户的token
        redisTemplate.delete(token);
        User user = JsonUtil.getInstance().jsonToObject(userJson, User.class);
        user.setToken("");
        log.info(user.getUsername() + "已退出登录");
        userMapper.updateById(user);
        return Result.success();
    }
}
