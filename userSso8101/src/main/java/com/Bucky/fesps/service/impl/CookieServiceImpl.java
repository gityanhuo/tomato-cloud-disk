package com.Bucky.fesps.service.impl;

import com.Bucky.fesps.service.CookieService;
import com.Bucky.fesps.utils.CookieUtil;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Danny
 * @date 2022-03-24
 */
@Service
public class CookieServiceImpl implements CookieService {


    /**
     * 设置cookie实现类
     *
     * @param request
     * @param response
     * @param token
     * @return
     */
    @Override
    public boolean setCookie(HttpServletRequest request, HttpServletResponse response, String token) {

        try {
            CookieUtil.setCookie(request, response, "UserToken", token);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
