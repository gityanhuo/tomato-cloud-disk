package com.Bucky.fesps.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Danny
 * @date 2022-03-24
 */
public interface CookieService {

    /**
     * 设置cookie
     * @param request
     * @param response
     * @param token
     * @return
     */
     boolean setCookie(HttpServletRequest request, HttpServletResponse response,String token);
}
