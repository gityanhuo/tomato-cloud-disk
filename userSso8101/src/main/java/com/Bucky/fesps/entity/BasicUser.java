package com.Bucky.fesps.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Date;

/**
 * @author Danny
 * @date 2022-03-17
 */
@TableName("user")
@Data
public class BasicUser {

    @TableId(type = IdType.AUTO)
//    private Integer id;
    private String username;
//    private String nickName;
//    private String email;
    private String password;
//    private String secretKey;        //密码加密后的密钥
//    private Date addTime;            //创建用户时间
//    private Date lastLogin ;         //最后登录的时间
//    private String lastIp;             //最后登录的ip
//    private String navlist;          //权限
//    private Date birthday;
//    private String sex;
//    private String address;
}
