package com.Bucky.fesps.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;


/**
 * @author Danny
 * @date 2022-03-17
 */
@TableName("user")
@Data
public class User {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String username;
    private String nickName;
    private String email;
    private String password;
    private String secretKey;        //密码加密后的密钥
    @TableField(fill = FieldFill.INSERT) //创建时自动填充时间
    private Date addTime;            //创建用户时间
    @TableField(fill = FieldFill.INSERT_UPDATE)//创建与修改时自动填充时间
    private Date lastLogin ;         //最后登录的时间
    private String lastIp;             //最后登录的ip
    private String navlist;          //权限
    private Date birthday;
    private String sex;
    private String address;
    //如果在线token不为空
    private String token;
}
