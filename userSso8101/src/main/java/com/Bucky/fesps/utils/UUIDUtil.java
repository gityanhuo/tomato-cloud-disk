package com.Bucky.fesps.utils;

import java.util.UUID;

/**
 * @author Danny
 * @date 2022-03-22
 * 生成UUID工具类
 */
public class UUIDUtil {

    public static String getUUID(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    };
}
