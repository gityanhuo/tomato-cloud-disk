package com.Bucky.fesps.services;

import com.Bucky.fesps.config.Result;

/**
 * @author Danny
 * @date 2022-05-18
 */
public interface BucketService {

    ////获取桶中文件总共的大小
    Result getBucket(String bucketName);
}
