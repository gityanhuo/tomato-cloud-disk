package com.Bucky.fesps.services.impl;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.config.SearchResult;
import com.Bucky.fesps.entity.File;
import com.Bucky.fesps.services.FileServices;
import com.Bucky.search.entity.FileSearchBean;
import com.Bucky.search.service.FileSearchServices;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.omg.CORBA.portable.OutputStream;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.Bucky.fesps.util.fileTypeUtil.FileNameAddType;
import static com.Bucky.fesps.util.fileTypeUtil.analysisFileType;

/**
 * @author Danny
 * @date 2022-03-26
 */
@Slf4j
@Service
public class FileServicesImpl implements FileServices {

    private final MinioClient minioClient;

    private FileSearchServices fileSearch;

    public FileServicesImpl(MinioClient minioClient, FileSearchServices fileSearch) {
        this.minioClient = minioClient;
        this.fileSearch = fileSearch;
    }


    //判断文件是否存在
    @Override
    public Result checkFileExists(String bucketName, String fileName) {
        try {
            StatObjectResponse statObject = minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(fileName).build());
            return Result.success(statObject);
        } catch (Exception e) {
            return Result.error("-1", fileName + "文件不存在");
        }
    }

    //上传文件
    @Override
    public Result fileUpload(MultipartFile[] multipartFiles, String bucketName) {
        boolean found;
        try {
            //判断桶存不存在
            found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!found) {
                //如果不存在就创建bucket
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
                log.info("bucket不存在，已创建" + bucketName + "桶");
            }
        } catch (Exception e) {
            log.info("Controller运行错误");
        }

        //文件上传
        for (MultipartFile f : multipartFiles) {
            InputStream is = null;
            //文件名
            //用一个字符来代替文件类型，最后加入到文件名最前面，既方便查询也方便分类 w:文档;t:图片；s:视频;y音频;q:其他;
            String fileTypeAndName = FileNameAddType(f.getOriginalFilename());


            try {
                //获取文件流
                is = f.getInputStream();


                minioClient.putObject(
                        PutObjectArgs.builder()
                                .bucket(bucketName)
                                .object(fileTypeAndName)
                                .stream(is, f.getSize(), -1)
                                .build());
                log.info("上传成功");
                //将文件添加进入全文检索系统
                String fileName = f.getOriginalFilename();
                long fileSize = f.getSize();
                String fileType=analysisFileType(FileNameAddType(fileName));
                fileSearch.addAndUpdate(new FileSearchBean(bucketName+fileName,bucketName,fileName,fileType,new Date(),fileSize));

            } catch (Exception e) {
                log.info("上传" + fileTypeAndName + "文件上传方法出现错误");
                return Result.error("-1", "文件上传方法出现错误");
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        log.info("文件上传流关闭失败");
                    }
                }
            }
//            //如果是图片，再上传一个缩略图
//            if(fileTypeAndName.substring(0,1).equals("t")){
//                Thumbnails.of(f).
//            }

        }
        return Result.success();

    }

    //获取文件的url
    @Override
    public String fileUrl(String fileName, String bucketName) {
        String url = "";
        try {
            url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.GET)
                    .bucket(bucketName)
                    .object(fileName)
                    .expiry(1, TimeUnit.DAYS)//设置url有效市场
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return url;
    }
    
    //下载文件
    @Override
    public Result fileDownload(HttpServletResponse response, String fileName, String bucketName){
        InputStream in = null;
        try {
            //获取文件对象

            StatObjectResponse stat = minioClient.statObject(
                    StatObjectArgs.builder()
                            .bucket(bucketName)
                            .object(fileName)
                            .build());
            response.setContentType(stat.contentType());
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName.substring(1), "UTF-8"));
            //文件下载
            in = minioClient.getObject(
                    GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(fileName)
                            //切块下载（预留）
//                            .offset()//开始未知
//                            .length()//长度
                            .build());
            IOUtils.copy(in, response.getOutputStream());
            log.info(fileName + "文件下载成功");
        } catch (Exception e) {
            log.info(fileName + "文件下载失败");
            return Result.error("-1",fileName.substring(1) + "文件下载失败");
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.info("输出流关闭失败");
                }
            }
        }

        return Result.success(fileName.substring(1)+"文件下载成功");

    }


    //删除文件
    @Override
    public Result fileDelete(String fileName, String bucketName) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(fileName).build());
            //在全文检索系统中删除
            fileSearch.removeFile(bucketName,fileName.substring(1));
        } catch (Exception e) {
            log.info("删除文件" + fileName + "出错");
            return Result.error("-1", "删除文件出错");
        }
        return Result.success();
    }

    //获取文件列表
    @Override
    public SearchResult<List<File>> fileList(String bucketName, String fileTag,long pageSize,long pageNum) throws Exception {

        Iterable<io.minio.Result<Item>> myObject;


        //获取buckeyt列表,如果fileTag不为空，则查询单一类型的列表
        myObject = minioClient.listObjects(ListObjectsArgs
                .builder()
                .bucket(bucketName)
                .prefix(fileTag)
                .build());

        Iterator<io.minio.Result<Item>> iterator = myObject.iterator();
        List<File> items = new ArrayList<>();
        //用于分页
        long total=0;
        while (iterator.hasNext()) {
            total++;
            Item item = iterator.next().get();
            if(pageNum!=-1&&(total<=(pageSize*(pageNum-1))||total>(pageSize*pageNum))){
                continue;
            }
//            if(tag>(pageSize*(pageNum-1))&&tag<(pageSize*pageNum))
            //将每个查询到的数据添加到集合中
            String fileName = item.objectName();
            Date modTime = null;
            String fileType = analysisFileType(fileName);
//            //删除文件夹（未开发）
            try {
                //如果是文件夹就会报错，这样就会直接跳过文件类型分析,就不会对文件类型再赋值。
                modTime = Date.from(item.lastModified().toInstant());
//                fileType = analysisFileType(fileName);
//
            } catch (Exception e) {
                fileName = fileName.replace("/", "");
            }
            //文件名去掉第一个类型字符
            if (fileType.length() < 4)
                fileName = fileName.substring(1);

//            String imgPath = "@/assets/images/icon/" + fileType + ".png";
            items.add(new File(fileType, fileName, modTime, item.size()));
        }
        return SearchResult.success(items,total);
    }

    //文件搜索
    @Override
    public SearchResult<List<FileSearchBean>> fileSearch(String fileName, String bucketName, int pageNum, int pageSize) {
        SearchResult<List<FileSearchBean>> result = fileSearch.searchFile(bucketName, fileName, pageNum, pageSize);
        return result;
    }

    //用户所有文件检索存入全文检索系统
    public Result fileCheck(String bucketName) throws Exception {
        //先删除该用户的所有文件
        fileSearch.deleteSingleUserFile(bucketName);
        //再将从文件系统查询的所有文件写入
        SearchResult<List<File>> files = fileList(bucketName, null,10,-1);
        FileSearchBean file = new FileSearchBean();
        for (File e : files.getData()) {
            fileSearch.addAndUpdate(new FileSearchBean(bucketName+e.getFileName(),bucketName,e.getFileName(),e.getFileType(),e.getModTime(),e.getFileSize()));
        }
        return Result.success();
    }


    //获取图片缩略图
    @Override
    public OutputStream getThumbnail(String bucketName, String fileName) throws Exception {
        //获取文件
        InputStream stream = minioClient.getObject(
                GetObjectArgs.builder()
                        .bucket(bucketName)
                        .object(fileName)
                        .build());


        OutputStream os = null;
        Thumbnails.of(stream).scale(1f)
                .outputFormat("jpg")
                .forceSize(40, 40)
                .toOutputStream(os);
        return os;
    }
}
