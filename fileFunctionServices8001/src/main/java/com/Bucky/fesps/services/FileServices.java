package com.Bucky.fesps.services;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.config.SearchResult;
import com.Bucky.fesps.entity.File;
import com.Bucky.search.entity.FileSearchBean;
import org.omg.CORBA.portable.OutputStream;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Danny
 * @date 2022-03-26
 */
public interface FileServices {

    //判断文件是否存在
    Result checkFileExists(String bucketName,String fileName);

    //上传文件
    Result fileUpload(MultipartFile[] multipartFiles,String bucketName);

    //获取文件的url
    String fileUrl(String fileName,String bucketName);

    //下载文件
    Result fileDownload(HttpServletResponse response, String fileName, String bucketName);

    //删除文件
    Result fileDelete(String fileName,String bucketName);

    //获取文件列表
    SearchResult<List<File>> fileList(String bucketName, String fileTag,long pageSize,long pageNum)throws Exception;

    //检索用户所有文件存入全文检索
    Result fileCheck(String bucketName) throws Exception;

    //文件搜索
    SearchResult<List<FileSearchBean>> fileSearch(String fileName, String bucketName, int pageNum, int pageSize);

    //获取文件缩略图
    OutputStream getThumbnail(String bucketName, String fileName) throws Exception;






}
