package com.Bucky.fesps.services;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Danny
 * @date 2022-03-25
 * <p>
 * name指定调用rest接口所对应的服务名
 * path指定调用rest接口所在的stockController指定的@RequestMapping("/user")
 */

@FeignClient(name = "user-services", path = "/user")
public interface UserFeignService {

    //声明需要调用的rest接口对应的方法
    @GetMapping("/test")
    String getTest();

    //验证token返回用户信息
    @ResponseBody
    @GetMapping("/validate")
    Result<User> validate();

//    //测试
//    @ResponseBody
//    @PostMapping("/login")
//    Result login(User user, HttpServletRequest request, HttpServletResponse response);
}
