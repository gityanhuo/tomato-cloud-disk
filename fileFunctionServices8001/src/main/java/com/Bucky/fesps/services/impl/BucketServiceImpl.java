package com.Bucky.fesps.services.impl;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.entity.File;
import com.Bucky.fesps.services.BucketService;
import io.minio.ListObjectsArgs;
import io.minio.MinioClient;
import io.minio.messages.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Danny
 * @date 2022-05-18
 */
@Service
public class BucketServiceImpl implements BucketService {

    @Autowired
    MinioClient minioClient;

    //获取桶中文件总共的大小
    @Override
    public Result getBucket(String bucketName) {

        try {
            Iterable<io.minio.Result<Item>> myObject;
            //获取buckeyt列表,如果fileTag不为空，则查询单一类型的列表
            myObject = minioClient.listObjects(ListObjectsArgs
                    .builder()
                    .bucket(bucketName)
                    .build());
            Iterator<io.minio.Result<Item>> iterator = myObject.iterator();
            List<File> items = new ArrayList<>();
            long size = 0;
            while (iterator.hasNext()) {
                size =size+ iterator.next().get().size();
            }
            return Result.success(size);
        }catch (Exception e){
            return Result.error("-1","获取桶存储情况错误");
        }

    }
}
