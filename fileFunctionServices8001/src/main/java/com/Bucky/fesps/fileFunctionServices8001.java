package com.Bucky.fesps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Danny
 * @date 2022-03-26
 */
@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
public class fileFunctionServices8001 {
    public static void main(String[] args) {
        SpringApplication.run(fileFunctionServices8001.class,args);
    }
}
