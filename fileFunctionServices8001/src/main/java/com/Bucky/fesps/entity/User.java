package com.Bucky.fesps.entity;

import lombok.Data;

import java.util.Date;


/**
 * @author Danny
 * @date 2022-03-17
 */

@Data
public class User {

    private Integer id;
    private String username;
    private String nickName;
    private String email;
    private String password;
    private String secretKey;        //密码加密后的密钥
    private Date addTime;            //创建用户时间
    private Date lastLogin;         //最后登录的时间
    private String lastIp;             //最后登录的ip
    private String navlist;          //权限
    private Date birthday;
    private String sex;
    private String address;
}
