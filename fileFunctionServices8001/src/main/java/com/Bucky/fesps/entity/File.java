package com.Bucky.fesps.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Danny
 * @date 2022-04-18
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class File {


    private String fileType;
    private String fileName;
    private Date modTime;
    private Long  fileSize;
//    private String imgPath;


}
