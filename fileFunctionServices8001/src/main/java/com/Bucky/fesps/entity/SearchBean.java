package com.Bucky.fesps.entity;

import lombok.Data;

/**
 * @author Danny
 * @date 2022-05-13
 */
@Data
public class SearchBean {

    private String search;
    private int pageNum;
    private int pageSize;
}
