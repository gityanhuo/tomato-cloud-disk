package com.Bucky.fesps.controller;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.config.SearchResult;
import com.Bucky.fesps.entity.File;
import com.Bucky.fesps.entity.SearchBean;
import com.Bucky.fesps.entity.User;
import com.Bucky.fesps.services.BucketService;
import com.Bucky.fesps.services.FileServices;
import com.Bucky.fesps.services.UserFeignService;
import com.Bucky.fesps.services.impl.FileServicesImpl;
import com.Bucky.fesps.util.fileTypeUtil;
import com.Bucky.search.entity.FileSearchBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Danny
 * @date 2022-03-26
 */
@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    final UserFeignService userFeignService;

    final FileServices fileServices;

    final BucketService bucketService;

    public FileController(UserFeignService userFeignService, FileServicesImpl fileServices, BucketService bucketService) {
        this.userFeignService = userFeignService;
        this.fileServices = fileServices;
        this.bucketService = bucketService;
    }


    //获取bucket的内存使用情况
    @GetMapping("/size")
    public Result getBucketSize(){

        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        Result bucket = bucketService.getBucket(bucketName);
        return bucket;
    }


    @PostMapping("/upload")
    public Result Upload( MultipartFile[] file) {
        Result<User> userResult = userFeignService.validate();
//        用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        Result result = fileServices.fileUpload(file, bucketName);

        return result;
    }

//    @GetMapping("/url/{fileName}")
//    public String fileUrl(@PathVariable("fileName") String fileName){
//        String url =null;
//        return url;
//    }


    @GetMapping("/download/{fileName}")
    public Result fileDownload(HttpServletResponse response, @PathVariable("fileName") String fileName)  {
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        //拼接类型和文件名
        String fileTypeAndName = fileTypeUtil.FileNameAddType(fileName);

        Result result = fileServices.fileDownload(response, fileTypeAndName, bucketName);


        return null;
    }


    @GetMapping("/delete/{fileName}")
    public Result fileDelete(@PathVariable("fileName") String fileName) {
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        //拼接类型和文件名
        String fileTypeAndName = fileTypeUtil.FileNameAddType(fileName);
        //如果是文件夹需要删除问价夹下所有的文件。
        if(fileTypeAndName.substring(0,1).equals("f")) {
            String folderName = fileTypeAndName + "/";

            try {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        fileServices.fileDelete(fileTypeAndName,bucketName);
        Result result = fileServices.checkFileExists(bucketName,fileTypeAndName);
        if(result.getCode().equals("-1")){
            log.info(fileName+"文件删除成功");
            return Result.success();
        }
        return Result.error("-1","删除文件出错");
    }


    //文件列表查询
    @GetMapping("/list")
    public SearchResult<List<File>> fileList(@RequestParam("pageNum") Long pageNum,@RequestParam("pageSize") Long pageSize) throws Exception {
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return SearchResult.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        SearchResult<List<File>> fileList = fileServices.fileList(bucketName,null,pageSize,pageNum);

        return fileList;
    }

    //检索当前用户的所有文件存入全文检索的部分
    @GetMapping("/check")
    public Result fileCheck() throws Exception {
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        Result result = fileServices.fileCheck(bucketName);
        return result;
    }


    //单一类型文件查询
    @GetMapping("/typeFileList")
    public SearchResult<List<File>> typeFileList(@RequestParam("fileTag") String fileTag,@RequestParam("pageNum") Long pageNum,@RequestParam("pageSize") Long pageSize) throws Exception {
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return SearchResult.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        SearchResult<List<File>> fileList = fileServices.fileList(bucketName,fileTag,pageSize,pageNum);

        return fileList;
    }

    //文件搜索
    @PostMapping("/search")
    public SearchResult fileSearch(@RequestBody(required = false)SearchBean searchBean){

        String search = searchBean.getSearch();
        int pageNum = searchBean.getPageNum();
        int pageSize = searchBean.getPageSize();
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return SearchResult.error("401", "账号未登录");
        }
        String userName = userResult.getData().getUsername();
        SearchResult<List<FileSearchBean>> listResult = fileServices.fileSearch(search, userName, pageNum, pageSize);
        return listResult;
    }

    @GetMapping("/getFileUrl/{fileName}")
    public Result getFileUrl(@PathVariable("fileName") String fileName){
        Result<User> userResult = userFeignService.validate();
        //用户验证
        if (userResult.getCode().equals("-1")) {
            //用户不在线，需要登陆
            return Result.error("401", "账号未登录");
        }
        String bucketName = userResult.getData().getUsername();
        //生成完整的文件名
        String fullFileName = fileTypeUtil.FileNameAddType(fileName);
        String fileUrl = fileServices.fileUrl(fullFileName, bucketName);
        if(fileUrl!=null){
            log.info(bucketName+"用户生成"+fileName+"时效为一天的url为"+fileUrl);
            return Result.success(fileUrl);
        }
        return Result.error("-1","url生成出错");


    }

}
