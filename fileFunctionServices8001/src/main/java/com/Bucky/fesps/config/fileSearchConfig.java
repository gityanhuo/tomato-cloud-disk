package com.Bucky.fesps.config;


import com.Bucky.fesps.dao.FileSearchMapper;
import com.Bucky.search.service.FileSearchServices;
import com.Bucky.search.service.impl.FileSearchImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

/**
 * @author Danny
 * @date 2022-05-13
 */
@Configuration
public class fileSearchConfig {

    @Autowired
    FileSearchMapper fileSearchDao;

    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Bean
    public FileSearchServices getFileSearch(){
        return new FileSearchImpl(fileSearchDao,elasticsearchRestTemplate);
    }

}
