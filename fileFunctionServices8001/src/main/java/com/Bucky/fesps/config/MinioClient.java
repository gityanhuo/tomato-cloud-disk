package com.Bucky.fesps.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Danny
 * @date 2022-03-04
 */
@Configuration
public class MinioClient {


    private MinioProperties minioProperties;

    @Autowired
    public MinioClient(MinioProperties minioProperties) {
        this.minioProperties = minioProperties;
    }

    @Bean
    public io.minio.MinioClient getMinioClient(){
        io.minio.MinioClient minioClient = io.minio.MinioClient.builder()
                .endpoint(minioProperties.getEndpoint())
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey()).build();
        return minioClient;
    }
}
