package com.Bucky.fesps.util;

/**
 * @author Danny
 * @date 2022-04-18
 */
public class fileTypeUtil {

    public static String FileNameAddType(String fileName) {
        //用一个字符来代替文件类型，最后加入到文件名最前面，既方便查询也方便分类 w:文档;t:图片；s:视频;y音频;q:其他;f:文件夹;
        char fileType;
        try {
            String nameType = fileName.split("\\.")[1];
            switch (nameType) {
                case "txt":
                case "docx":
                case "doc":
                case "pptx":
                case "ppt":
                case "xlsx":
                case "xls":
                case "md":
                    fileType = 'w';
                    break;
                case "jpeg":
                case "jpg":
                case "png":
                case "gif":
                case "tif":
                case "tga":
                case "bmp":
                case "dds":
                case "svg":
                case "eps":
                case "pdf":
                case "hdr":
                case "raw":
                case "exr":
                case "psd":
                    fileType = 't';
                    break;
                case "mp4":
                case "flv":
                case "f4v":
                case "webm":
                case "m4v":
                case "mov":
                case "3gp":
                case "3g2":
                case "rm":
                case "rmvb":
                case "wmv":
                case "avi":
                case "asf":
                case "mpg":
                case "mpeg":
                case "mpe":
                case "ts":
                case "div":
                case "dv":
                case "divx":
                case "vob":
                case "dat":
                case "mkv":
                case "lavf":
                case "cpk":
                case "dirac":
                case "ram":
                case "qt":
                case "fli":
                case "flc":
                case "mod":
                    fileType = 's';
                    break;
                case "mp3":
                case "wav":
                case "aiff":
                case "pcm":
                case "flac":
                case "wma":
                case "ogg":
                case "aac":
                    fileType = 'y';
                    break;
                default:
                    fileType = 'q';
                    break;
            }
        } catch (Exception e) {
            //文件夹没有后缀
            fileType = 'f';
        }
        String fileTypeAndName = fileType + fileName;

        return fileTypeAndName;
    }


    public static String analysisFileType(String fileTypeAndName) {
        String fileTypeTag = fileTypeAndName.substring(0, 1);
        String fileType;
        switch (fileTypeTag) {
            case "w":
                fileType = "文档";
                break;
            case "t":
                fileType = "图片";
                break;
            case "s":
                fileType = "视频";
                break;
            case "y":
                fileType = "音频";
                break;
            case "q":
                fileType = "其他";
                break;
            case "f":
                fileType = "文件夹";
                break;
            default:
                fileType = "解析出错";
                break;
        }
        return fileType;
    }
}
