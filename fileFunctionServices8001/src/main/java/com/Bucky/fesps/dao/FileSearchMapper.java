package com.Bucky.fesps.dao;

import com.Bucky.search.dao.FileSearchDao;
import org.springframework.stereotype.Repository;

/**
 * @author Danny
 * @date 2022-05-14
 */
@Repository
public interface FileSearchMapper extends FileSearchDao {
}
