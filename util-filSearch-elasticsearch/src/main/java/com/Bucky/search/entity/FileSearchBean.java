package com.Bucky.search.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Danny
 * @date 2022-04-27
 * spring data for apache solr识别实体的时候，属性名和字段名一一对应。
 * SolrDocument注解：当前实体对应一个Solr的索引库
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(indexName = "tomato")
public class FileSearchBean implements Serializable {

    @Id
    private String id;
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String userName;
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String fileName;
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String fileType;
    @Field(type = FieldType.Date)
    private Date modTime;
    @Field(type = FieldType.Long)
    private long fileSize;

    public FileSearchBean(String id) {
        this.setId(id);
    }

}
