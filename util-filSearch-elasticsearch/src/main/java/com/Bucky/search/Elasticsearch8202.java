package com.Bucky.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Danny
 * @date 2022-05-07
 */
@SpringBootApplication
public class Elasticsearch8202 {
    public static void main(String[] args) {
        SpringApplication.run(Elasticsearch8202.class,args);
    }
}
