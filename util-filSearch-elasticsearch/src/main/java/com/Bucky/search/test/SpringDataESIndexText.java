package com.Bucky.search.test;

import com.Bucky.search.dao.FileSearchDao;
import com.Bucky.search.entity.FileSearchBean;
import com.Bucky.search.service.impl.FileSearchImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;

import java.util.Date;

/**
 * @author Danny
 * @date 2022-05-08
 */

@SpringBootTest
public class SpringDataESIndexText {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Test
    public void createIndex(){
        System.out.println("创建索引");
    }

    @Test
    public void deleteIndex(){

        boolean file = elasticsearchRestTemplate.indexOps(IndexCoordinates.of("tomato")).delete();
        System.out.println(file+"删除索引");
    }

    @Autowired
    private FileSearchDao fileSearchDao;

    @Test
    public void save(){
        FileSearchBean file = new FileSearchBean();
        file.setId("1测试");
        file.setUserName("root");
        file.setFileName("测试");
        file.setModTime(new Date());
        file.setFileSize(100L);
        file.setFileType("视频");
        fileSearchDao.save(file);
    }

    @Autowired
    FileSearchImpl search ;

    @Test
    public void searchTest(){

        


//        search.deleteSingleUserFile("1");


//        FileSearchBean fileSearchBean = new FileSearchBean();
//        fileSearchBean.setUserName("1");
//
//
//        elasticsearchRestTemplate.delete(fileSearchBean);

//        FileSearchBean file = new FileSearchBean();
//        file.setId("1");
//        file.setUserName("1");
//        file.setFileName("测试");
//        file.setModTime(new Date());
//        file.setFileSize(100L);
//        search.addAndUpdate(file);

//        search.removeFile("1","测试");

//        //查询条件--多条件
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        //条件：必须
//        boolQueryBuilder.must(QueryBuilders.matchQuery("userName","root"));
//
//        //搭建条件构建器
//        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
//                .withQuery(boolQueryBuilder)
//                //模糊查询
//                .withQuery(QueryBuilders.queryStringQuery("测试").defaultField("fileName"))
//                .withPageable(PageRequest.of(0,10))
//                .build();
//
//        //搜索结果
//        SearchHits<FileSearchBean> search = elasticsearchRestTemplate.search(searchQuery, FileSearchBean.class);
//
//        search.getSearchHits().forEach(e-> System.out.println(e));

//        Result<List<FileSearchBean>> listResult = search.searchFile("root", "测试", 1, 10);
//        listResult.getData().forEach(e-> System.out.println(e));

    }

}
