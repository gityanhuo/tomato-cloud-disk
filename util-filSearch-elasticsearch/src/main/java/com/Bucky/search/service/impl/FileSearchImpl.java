package com.Bucky.search.service.impl;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.config.SearchResult;
import com.Bucky.search.dao.FileSearchDao;
import com.Bucky.search.entity.FileSearchBean;
import com.Bucky.search.service.FileSearchServices;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danny
 * @date 2022-05-10
 */
@Slf4j
@Component
public class FileSearchImpl implements FileSearchServices {

    private final FileSearchDao fileSearchDao;

    private final ElasticsearchRestTemplate elasticsearchRestTemplate;

    public FileSearchImpl(FileSearchDao fileSearchDao, ElasticsearchRestTemplate elasticsearchRestTemplate) {
        this.fileSearchDao = fileSearchDao;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
    }

    //删除索引
    public boolean deleteIndex(String indexName) {
        boolean result = elasticsearchRestTemplate.indexOps(IndexCoordinates.of(indexName)).delete();
        return result;
    }

    //添加和更新文件
    @Override
    public Result addAndUpdate(FileSearchBean fileSearchBean) {
        fileSearchDao.save(fileSearchBean);
        return Result.success();

    }


    //删除文件
    @Override
    public Result removeFile(String userName, String FileName) {
        elasticsearchRestTemplate.delete(new FileSearchBean(userName + FileName));
        log.info("在elasticsearch中删除 " + userName + " 用户的 " + FileName + " 文件");
        return Result.success();
    }

    //删除单一用户的所有文件
    public Result deleteSingleUserFile(String userName) {
        NativeSearchQuery userQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.queryStringQuery(userName).defaultField("userName"))
                .build();

        SearchHits<FileSearchBean> userFileSearch = elasticsearchRestTemplate.search(userQuery, FileSearchBean.class);
        for (SearchHit<FileSearchBean> e : userFileSearch.getSearchHits()) {
            if(e.getContent().getUserName().equals(userName))
                elasticsearchRestTemplate.delete(e.getContent());
        }
        return Result.success();
    }


    //搜索文件
    @Override
    public SearchResult<List<FileSearchBean>> searchFile(String userName, String fileName, int page, int size) {
        //模糊搜索条件
        //es的页码从0开始，所以需要减一
        page--;

        //查询条件--多条件
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //条件：必须
//        boolQueryBuilder.must(QueryBuilders.matchQuery("userName", userName));

        //搭建条件构建器
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
//                .withQuery(boolQueryBuilder)
                //模糊查询
//                .withQuery(QueryBuilders.queryStringQuery(fileName).defaultField("fileName"))
                .withQuery(QueryBuilders.matchQuery("fileName",fileName))
                .withPageable(PageRequest.of(page, size))
                .build();

        //搜索结果
        SearchHits<FileSearchBean> search = elasticsearchRestTemplate.search(searchQuery, FileSearchBean.class);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List list = new ArrayList<FileSearchBean>();
        long total = search.getTotalHits();
        search.getSearchHits().forEach(e -> {
            if (e.getContent().getUserName().equals(userName))
                list.add(e.getContent());
        });
        SearchResult<List<FileSearchBean>> result = SearchResult.success(list, total);
        return result;
    }


}
