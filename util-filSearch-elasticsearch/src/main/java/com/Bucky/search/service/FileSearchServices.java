package com.Bucky.search.service;

import com.Bucky.fesps.config.Result;
import com.Bucky.fesps.config.SearchResult;
import com.Bucky.search.entity.FileSearchBean;

import java.util.List;

/**
 * @author Danny
 * @date 2022-05-08
 */

public interface FileSearchServices {

    //添加文件
    Result addAndUpdate(FileSearchBean fileSearchBean);

    //删除文件
    Result removeFile(String userName,String FileName);

    //搜索文件
    SearchResult<List<FileSearchBean>> searchFile(String userName, String FileName, int page, int size);

    //删除索引
    boolean deleteIndex(String indexName);

    //删除单一用户的所有文件
    Result deleteSingleUserFile(String userName);

}
