package com.Bucky.search.dao;

import com.Bucky.search.entity.FileSearchBean;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Danny
 * @date 2022-05-08
 */
@Repository
public interface FileSearchDao extends ElasticsearchRepository<FileSearchBean,String> {

}
